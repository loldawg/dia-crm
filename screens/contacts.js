import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ScrollView,
 } from 'react-native';
 import {
   Header,
   SearchBar,
   List,
   ListItem,
} from 'react-native-elements';

import Contactsicon from '../components/contactsicon'
import Backicon from '../components/backicon'



const list = [
  {
    fullname: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
  {
    fullname: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  }
]

export default class Contacts extends React.Component {
  static navigationOptions = {
    header: null,
  }
  render() {
    return (
      <View style={styles.container}>
        <Header
          leftComponent={ <Backicon nav={this.props.navigation}/> }
          centerComponent={{ text: 'Contacts', style: { color: '#fff' } }}
          rightComponent={ <Contactsicon /> }
          outerContainerStyles={styles.outerContainer}
          statusBarProps={{ backgroundColor: '#017bc7' }}
          backgroundColor='#017bc7'
        />
        <SearchBar
          placeholder='Type Here...'
          containerStyle={styles.searchholder}
          inputStyle={styles.searchinput}
          icon={{style: styles.searchicon}}
          lightTheme
        />
        <View style={styles.gridcontainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <List containerStyle={styles.list}>
          {
            list.map((list, i) => (
              <ListItem
                roundAvatar
                avatar={{uri:list.avatar_url}}
                key={i}
                title={list.fullname}
                subtitle={list.subtitle}
                containerStyle={ styles.listitem }
                titleStyle={styles.listtitle}
                subtitleStyle={styles.listsubtitle}
                hideChevron
              />
            ))
          }
          </List>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f7f9',
  },
  searchholder: {
    backgroundColor: '#FFF',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 1
  },
  searchinput:{
    backgroundColor: 'transparent',
  },
  list: {
    marginBottom: 20,
    flex: 1,
    borderTopWidth: 0,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 1,
  },
  listitem: {
    borderBottomColor: '#f1f2f5',
    borderRadius: 5,
  },
  listtitle: {
    color: '#7b8591',
  },
  listsubtitle: {
    color: '#afb8c2',
    fontWeight: 'normal',
    lineHeight: 9,
  },
  outerContainer: {
    position: 'relative',
    marginBottom: 0,
    borderBottomWidth: 0,
  },
  gridcontainer: {
    marginTop: -5,
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
  }
});
