import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ScrollView,
 } from 'react-native';
 import {
   Header,
   SearchBar,
   Button,
} from 'react-native-elements';

import Tile from '../components/tile';
import Headericon from '../components/headericon';





class BackButton extends React.Component {
  render(){
    return(
      <View>
        <Text>ooo</Text>
      </View>
    )
  }
}

export default class HomeScreen extends React.Component {
  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Header
          leftComponent={ <Headericon /> }
          centerComponent={{ text: 'Home', style: { color: '#fff' } }}
          rightComponent={{ icon: 'menu', color: '#fff' }}
          outerContainerStyles={styles.outerContainer}
          statusBarProps={{ backgroundColor: '#017bc7' }}
          backgroundColor='#017bc7'
        />
        <SearchBar
          placeholder='Type Here...'
          containerStyle={styles.searchholder}
          inputStyle={styles.searchinput}
          icon={{style: styles.searchicon}}
          lightTheme
        />
        <ScrollView contentContainerStyle={styles.scrolloffset}>
        <View style={styles.gridcontainer}>
          <Tile
          title='Contacts'
          icon='Contacts'
          page='Contacts'
          nav={this.props.navigation}
          />
          <Tile
          title='Organizations'
          icon='Organizations'
          />
          <Tile
          title='Countries'
          icon='Countries'
          />
          <Tile
          title='Projects'
          icon='Projects'
          />
          <Tile
          title='Embassies & Consulates'
          icon='Embassy'
          />
          <Tile
          title='Scan'
          icon='Scan'
          />
        </View>
        <Button
          title='Log out'
          containerViewStyle={styles.buttoncontainer}
          buttonStyle={styles.homebutton}
          textStyle={styles.buttontext}
        />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f7f9',
  },
  searchholder: {
    backgroundColor: '#FFF',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 1,
    zIndex: 5,
    position: 'relative'
  },
  searchinput: {
    backgroundColor: 'transparent',
  },
  searchicon: {
  },
  outerContainer: {
    position: 'relative',
    marginBottom: 0,
    borderBottomWidth: 0,
  },
  gridcontainer: {
    alignSelf: 'center',
    width: 350,
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10
  },
  homebutton: {
    backgroundColor: 'transparent',
  },
  buttontext: {
    color: '#bfc6cf',
    textAlign: 'center',
  },
  buttoncontainer: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#bfc6cf',
    borderRadius: 30,
    backgroundColor: 'transparent',
    width: 120,
    height: 45,
    alignSelf: 'center',
  },
  scrolloffset: {
    paddingBottom: 20,
  },
});
