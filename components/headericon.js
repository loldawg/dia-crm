import React, { Component } from 'react'

import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';


export default class Headericon extends Component {
  render(){
    return(
      <View style={styles.container}>
        <Image source={require('../assets/icons/usericon.png')} style={styles.icon} />
        <Text style={styles.user}> Hans </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
  },
  container: {
    width: 38,
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  user: {
    color: '#FFF',
  },
})
