import React, { Component } from 'react'

import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native';


export default class Backicon extends Component {
  render(){
    return(
      <View style={styles.container}>
      <TouchableHighlight onPress={ () => { this.props.nav && this.props.nav.goBack()  } } underlayColor='transparent'>
        <Image source={require('../assets/icons/backicon.png')} style={styles.icon} />
      </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
  },
  container: {
    width: 38,
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  user: {
    color: '#FFF',
  },
})
