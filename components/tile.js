import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  Alert,
 } from 'react-native';

 const icons = {
   Contacts: require('../assets/images/Contacts.png'),
   Countries: require('../assets/images/Countries.png'),
   Embassy: require('../assets/images/Embassy.png'),
   Organizations: require('../assets/images/Organizations.png'),
   Projects: require('../assets/images/Projects.png'),
   Scan: require('../assets/images/Scan.png'),
}



class Tile extends Component {
  render(){
    var imgLink = icons[this.props.icon];
    return(
      <TouchableHighlight onPress={ () => { this.props.page && this.props.nav.navigate(this.props.page)  } } underlayColor='transparent' style={styles.touch}>
        <View style={styles.grid} onCLick>
          <Image source={ imgLink } style={styles.tileicon} />
          <Text style={styles.tiletitle}> {this.props.title} </Text>
        </View>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  grid: {
    margin: 2,
    width: 170,
    height: 170,
    backgroundColor: '#fff',
    elevation: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tileicon:{
    maxHeight: 90,
    maxWidth: 90,
    alignSelf: 'center',
  },
  tiletitle:{
    alignSelf: 'center',
    color: '#373535',
    marginTop: -5,
    width: 100,
    textAlign: 'center'
  },
  touch: {
    borderWidth: 0,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
  }
});

export default Tile;
