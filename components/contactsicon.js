import React, { Component } from 'react'

import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';


export default class Contactsicon extends Component {
  render(){
    return(
      <View style={styles.container}>
        <Image source={require('../assets/icons/plusicon.png')} style={styles.icon} />
        <Image source={require('../assets/icons/menuicon.png')} style={styles.icon} />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  container: {
    width: 50,
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 20,
  },
  user: {
    color: '#FFF',
  },
})
