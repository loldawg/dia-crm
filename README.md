##DIA-CRM

This project was created with Expo, but can't be found there, since it's a private repo. Here's how to get it running locally:

1) Make sure you have `Node.js` installed on your system  
2) `git clone https://bitbucket.org/loldawg/dia-crm`  
3) `yarn upgrade` to install everything  
4) `yarn start` to run it locally  
5) Grab Expo client app for [Android](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en) or [iOS](https://itunes.apple.com/us/app/expo-client/id982107779?mt=8)  
6) Follow instructions from the terminal  

You can check the app with Expo client via WiFi, just make sure you're on the same network as your machine.
Wanna run it on a virtual device? `yarn ios` or `yarn android` got you covered. All comands should be compatible
with `npm` as well.
