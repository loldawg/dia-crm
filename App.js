import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
 } from 'react-native';
 import {
   Header,
   SearchBar,
   List,
   ListItem,
} from 'react-native-elements';


import HomeScreen from './screens/home';
import Contacts from './screens/contacts';
import { StackNavigator } from 'react-navigation';



const HomeScreenRouter = StackNavigator({
    Home: { screen: HomeScreen },
    Contacts: { screen: Contacts },
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
    // transitionConfig: () => ({ screenInterpolator: () => null }),
  }

)



export default HomeScreenRouter
